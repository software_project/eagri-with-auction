import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostauctionComponent } from './postauction.component';

describe('PostauctionComponent', () => {
  let component: PostauctionComponent;
  let fixture: ComponentFixture<PostauctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostauctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostauctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
