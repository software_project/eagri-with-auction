import { Component, OnInit } from '@angular/core';
import{FormControl, FormGroup, Validators, FormBuilder, NgForm} from'@angular/forms';
import { HttpClient } from '@angular/common/http';
import {PostauctionService} from '../shared/postauction.service';



@Component({
  selector: 'app-postauction',
  templateUrl: './postauction.component.html',
  styleUrls: ['./postauction.component.css']
})
export class PostauctionComponent implements OnInit {
  
  

  auctionForm= new FormGroup({
    _id:new FormControl(''),
    harvestVillage:new FormControl(''),
    auctionOwner: new FormControl(''),
    productName: new FormControl(''),
    Quantity: new FormControl(''),
    minimumPrice: new FormControl(''),
    harvestingDate:new FormControl(''),
    
    });
  

  constructor(private postauctionservice : PostauctionService) { }

  ngOnInit() {

    this.resetForm();
    
  }

  resetForm( form?: FormGroup){
    if(form)
    form.reset();
    this.postauctionservice.selectedPostAuction={
      _id:"",
      harvestVillage:"",
      auctionOwner:"",
      productName:"",
      Quantity:null,
      minimumPrice:null,
      harvestingDate:null,
      
      }


  }

  onSubmit(form :FormGroup){

    this.postauctionservice.postNewAuction(form.value).subscribe((res)=>{

      this.resetForm(form);
  

    }
    )}}
