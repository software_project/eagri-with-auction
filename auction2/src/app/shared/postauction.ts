import { Time } from '@angular/common';

export class Postauction {

    _id:string;
    harvestVillage:string;
    auctionOwner:string;
    productName:string;
    Quantity:number;
    harvestingDate:Date;
    minimumPrice:number;
    
    
}
