import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';



import {Postauction} from '../shared/postauction';
import {PostauctionService} from '../shared/postauction.service';



@Injectable({
  providedIn: 'root'
})
export class ViewauctionsService {
selectedPostAuction:Postauction;
allauctions:Postauction[];
  readonly  url_getAllAuc='http://localhost:3000/auctions/get-all'

  

  constructor(private http: HttpClient) { }


 getAllauctions(){

  return this.http.get(this.url_getAllAuc);
 }

 
}
