import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import{ Observable} from 'rxjs';
import{ map} from 'rxjs/Operators';



import {Postauction} from '../shared/postauction';

@Injectable({
  providedIn: 'root'
})
export class PostauctionService {
  selectedPostAuction:Postauction;
  postedAuctions:Postauction[];
  
 readonly url_saveAuc='http://localhost:3000/auctions/save-auction'



  constructor(private http : HttpClient) { }

  postNewAuction(auc :Postauction){

    return this.http.post(this.url_saveAuc,auc);
  }



}



