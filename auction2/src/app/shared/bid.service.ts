import { Injectable, APP_ID } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


import { Bid} from '../shared/bid.model';
import {Postauction} from '../shared/postauction';



@Injectable({
  providedIn: 'root'
})
export class BidService {
  selectedBid:Bid;
  id:string;
  aid
  readonly Baseurl='http://localhost:3000/auctions/add-bids';
  
  readonly url_getAucDetails='http://localhost:3000/auctions';

  readonly url_highBid='http://localhost:3000/auctions/maximunBid'



  constructor( private http:HttpClient) { }

  

  addNewbid(id,nic, vv){

    
    const ss={
    aid : id,
   bidderNIC :nic,
   bidValue:vv
    }


      return this.http.post(<any> this.Baseurl,ss );

  }

  getHighBid(){

    aid:this.id;
   return  this.http.get( this.getHighBid+`/${this.aid}`);
  }

} 
