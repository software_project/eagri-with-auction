import { TestBed } from '@angular/core/testing';

import { ViewauctionsService } from './viewauctions.service';

describe('ViewauctionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewauctionsService = TestBed.get(ViewauctionsService);
    expect(service).toBeTruthy();
  });
});
