import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { Router, RouterLink } from '@angular/router';


import{ViewauctionsService} from '../shared/viewauctions.service';
import {Postauction} from '../shared/postauction';
import {PostauctionService} from '../shared/postauction.service';
import { PostauctionComponent } from '../postauction/postauction.component';



@Component({
  selector: 'app-viewauctions',
  templateUrl: './viewauctions.component.html',
  styleUrls: ['./viewauctions.component.css']
})
export class ViewauctionsComponent implements OnInit {
  
 
  list:Postauction[]
  
  constructor(private viewauctionsservice: ViewauctionsService, private router:Router) { }
  

  ngOnInit( ) {

    this.viewauctionsservice.getAllauctions().subscribe((res)=>{


      this.list=res['data']['AuctionList'];
      console.log(this.list);
        })

  
  }
  

onClick(id:string){

  console.log(id);
  localStorage.setItem('id',id);
  
  this.router.navigate( <any>[`/bid/${id}`])
}



}
