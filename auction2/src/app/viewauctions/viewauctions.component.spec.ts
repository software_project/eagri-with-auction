import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewauctionsComponent } from './viewauctions.component';

describe('ViewauctionsComponent', () => {
  let component: ViewauctionsComponent;
  let fixture: ComponentFixture<ViewauctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewauctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewauctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
