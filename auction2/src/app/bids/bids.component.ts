import { Component, OnInit, ViewChild } from '@angular/core';
import { CounterComponent } from '../counter/counter.component';
import { FormGroup, FormControl} from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Bid} from '../shared/bid.model';

import {Router, ActivatedRoute} from '@angular/router';


import {BidService} from '../shared/bid.service';

@Component({
  selector: 'app-bids',
  templateUrl: './bids.component.html',
  styleUrls: ['./bids.component.css']
})
export class BidsComponent implements OnInit {

  
  // addbidForm= new FormGroup({

  //   id:new FormControl(''),
  //   bidderNIC: new FormControl(''),
  //   bidValue: new FormControl(''),
  // });

  


  @ViewChild('counter',{ read:CounterComponent,static:true})

 private counter:CounterComponent

  constructor(private bidservice:BidService, private route:ActivatedRoute) { }


  // id:string;

  // town:string;

  // product: string;

bidderNIC
bidValue
 a_id

 response

  ngOnInit() {


    this.route.params.subscribe(params=>{
      this.a_id=params.id;
      console.log('ddd:'+params.id);
      })

  //  this.addbidForm= new FormGroup({
  //   aid: new FormControl(''),
  //   bidderNIC: new FormControl(''),
  //   bidValue:new FormControl('')

  //  })
    
  

     this.resetForm();
      this.highBid();

    
    this.counter.startAt=1800
   this.counter.Start();
  }

   resetForm(form?:FormGroup){
   if (form)
  
    

       this.a_id.reset(), 
       this.bidderNIC.reset(),
      this.bidValue.reset()


   }

  onSubmit(form:FormGroup){

    this.bidservice.addNewbid(this.a_id,this.bidderNIC,this.bidValue).subscribe((res)=>{

      this.resetForm();
    })



  }

  highBid(){

    this.bidservice.getHighBid().subscribe((res)=>{

      this.response= res['data']



    })
  }


  

}
