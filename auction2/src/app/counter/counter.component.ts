import { Component, OnInit, Output, ChangeDetectionStrategy,ChangeDetectorRef, Input, EventEmitter } from '@angular/core';
import {Observable, interval, Subscription} from 'rxjs';
import {take} from 'rxjs/operators';
import   'rxjs/add/operator/map';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit {

  @Input()
 
  startAt=0;



  @Output()
 
  counterState= new EventEmitter<string>();

  currentValue='';

  currentSubscription:Subscription;

constructor(private changeDetecter:ChangeDetectorRef) { }

  ngOnInit() {
  }
 public Start(){

   this.currentValue=this.formatValue(this.startAt);
    this.changeDetecter.detectChanges();


    const t:Observable<number>= interval(1000);

   this.currentSubscription=t.pipe(take(this.startAt)).map(v=>this.startAt-v).subscribe(v=>{
      this.currentValue=this.formatValue(v);
      this.changeDetecter.detectChanges();

    }, (err)=>{

      this.counterState.error(err);

    }, ()=>{

      this.currentValue='00:00';
      this.counterState.emit('COMPLETE');
      this.changeDetecter.detectChanges();
    })
  }

  public Stop(){

    this.currentSubscription.unsubscribe();
   this.counterState.emit('ABORTED');
  }


    private formatValue(v){

     const minutes=Math.floor(v/60);
     const formattedMinutes=(minutes>9?minutes:'0'+minutes);

      const seconds= Math.floor(v%60);
      const formattedSeconds=(seconds>9? seconds:'0'+seconds);

      return `${formattedMinutes}:${formattedSeconds}`;
    }

 }