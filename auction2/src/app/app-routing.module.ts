import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PostauctionComponent } from './postauction/postauction.component';
import { ViewauctionsComponent } from './viewauctions/viewauctions.component';
import { BidsComponent } from './bids/bids.component';
import{ CounterComponent} from './counter/counter.component'




const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'postauction',component:PostauctionComponent},
  {path:'viewauctions',component:ViewauctionsComponent},
  {path:'home',component:HomeComponent},
  {path:'bid/:id',component:BidsComponent},
 {path: 'counter',component:CounterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
